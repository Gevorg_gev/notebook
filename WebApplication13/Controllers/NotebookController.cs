using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication13
{
    [ApiController]
    [Route("[controller]")]
    public class NotebookController : ControllerBase
    {
        private static readonly string[] Notebook_Name = new[]
        {
            "Smasung Galaxy Book S" , "Apple Macbook Pro 16" , "Samsung Galaxy Book Flex" , "Asus VivoBook 15" 
        };

        private static readonly string[] Notebook_Country = new[]
        {
            "South Korea" , "United States Of America" , "Thailand"
        };

        private static readonly string[] Notebook_ScreenResolution = new[]
        {
            "720p - HD " , "1080p - Full HD" , "1440p - Quad HD" , "2160p - Ultra HD" 
        };

        private static readonly string[] Notebook_Color = new[]
        {
            "Space Grey" , "Black" , "Silver" , "Pink" 
        };

        [HttpGet]
        public IEnumerable<Notebook> GetInfo()
        {
            var rng = new Random();
            return Enumerable.Range(1, 10).Select(index => new Notebook
            {
                Id = rng.Next(1,40),
                Name = Notebook_Name[rng.Next(Notebook_Name.Length)],
                Country = Notebook_Country[rng.Next(Notebook_Country.Length)],
                RAM = rng.Next(8,64),
                Memory = rng.Next(128,2048),
                SSDCapacity = rng.Next(128,512),
                HDDCapacity = rng.Next(256,4000),
                ScreenResolution = Notebook_ScreenResolution[rng.Next(Notebook_ScreenResolution.Length)],
                Color = Notebook_Color[rng.Next(Notebook_Color.Length)],
                Year = rng.Next(2019,2021) 
            });
        }
    }
}
