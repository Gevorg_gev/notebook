using Microsoft.EntityFrameworkCore;

namespace WebApplication13
{
    public class NotebookDbContext : DbContext
    {
        public DbSet<Notebook> Notebooks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=LAPTOP-R91MV32G;Database=NotebookDb;Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Notebook>(x=>
            {
                x.HasKey(x=>x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd(); 
            });
            base.OnModelCreating(modelBuilder);
        }
    } 
}
