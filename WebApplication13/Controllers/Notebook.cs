namespace WebApplication13
{
    public class Notebook
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public int RAM { get; set; }
        public int Memory { get; set; }
        public int SSDCapacity { get; set; }
        public int HDDCapacity { get; set; }
        public string ScreenResolution { get; set; }
        public string Color { get; set; }
        public int Year { get; set; } 
    }
}